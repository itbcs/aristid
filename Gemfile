source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# Base gems
gem 'rails', '~> 5.1.6'
gem 'puma', '~> 3.7'
gem 'puma-heroku'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'

# BDD
gem 'bson_ext'
gem 'mongo'
gem 'mongoid'
gem 'mongoid-history'
gem 'mongoid_orderable'
gem 'mongoid_auto_increment'

# Tracking logs and errors
gem 'newrelic_rpm'

# Geocoding + Autocomplete
gem "select2-rails"
gem "geocoder"

# Front gems
gem 'bootstrap', '~> 4.3.1'
gem 'modernizr-rails'
gem 'autoprefixer-rails'
gem 'gon'

# Handle authentification
gem "devise"

# Handle french phones
gem 'phony_rails'

# Handle forms
gem 'simple_form'

# Use haml as preprocessor
gem "haml-rails"

# Handle services processes
gem 'simple_command'

# JS framework
gem 'jquery-rails'

# Execute http requests
gem "rest-client"


group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
