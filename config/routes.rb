Rails.application.routes.draw do
  root to: 'journey#geoloc_start'
  get "agence/:lname", to: "journey#geoloc_start", as: "agency_start_page"
  get "scroll", to: "pages#scroll"

  namespace :api do
    get "agency/details", to: "agencies#show", as: "agency"
  end

  namespace :dashboard do
    get "start", to: "agencies#start", as: "agency"
    get "surveys", to: "agencies#surveys", as: "surveys"
    get "agent_creation", to: "agencies#create_agent", as: "create_agent"
    get "agency_agents", to: "agencies#agents", as: "agency_agents"
    get "survey/:id", to: "agencies#survey", as: "survey"
    resources :agents
  end

  devise_for :customers, controllers: { sessions: 'customers/sessions', registrations: 'customers/registrations' }, path_names: { sign_in: 'connexion', sign_up: "inscription" }
  devise_for :agencies, controllers: { sessions: 'agencies/sessions', registrations: 'agencies/registrations' }, path_names: {sign_in: 'connexion', sign_up: "inscription" }
  devise_for :agents, controllers: { sessions: 'agents/sessions', registrations: 'agents/registrations' }, path_names: {sign_in: 'connexion', sign_up: "inscription" }

  resources :questions
  resources :answers
  resources :conditions
  resources :sentences
  resources :front_conditions

  post "questions/test", to: "questions#test", as: "test"

  get "journey/geoloc_start", to: "journey#geoloc_start", as: "geoloc_start_journey"
  post "questionnaire(/:lname)", to: "journey#start", as: "start_journey"
  post "next_question", to: "journey#next_question", as: "next_question"
  post "next_question_skip", to: "journey#next_question_skip", as: "next_question_skip"
  get "journey/resume/:id", to: "journey#resume", as: "resume"
  post "change_answer_order", to: "answers#change_order", as: "change_answer_order"

  get "export_select_values", to: "conditions#export_select_values", as: "export_select_values"

  get "/404", to: "errors#not_found", as: "not_found"
  get "/500", to: "errors#internal_error", as: "internal_error"

  get '*unmatched_route', to: 'application#not_found_redirect'

end
