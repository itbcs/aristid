class SetSurveyScoring
  prepend SimpleCommand

  def initialize(customer)
    @customer = customer
    @survey = @customer.survey
  end

  def call
    @survey.survey_scoring? ? display_current_scoring : calculate_scoring
  end

  private

  def display_current_scoring
    @survey.survey_scoring
  end

  def calculate_scoring
    @scoring = SurveyScoring.new(survey: @survey, experience: experience.to_i, maturity: maturity.to_i, flexibility: flexibility.to_i, personality: personality.to_h)
    if @scoring.save
      @scoring
    else
      errors.add(:base, :failure)
    end
  end

  def experience
    puts "---- Inside experience ----"
    @experience_points = 0
    fields = [:actual_situation, :current_location, :age, :installation_date]
    @dynamic_coef = 100.0 / (fields.size.to_f * 5.0)
    fields.each do |field|
      value = @survey[field.to_sym]
      if value
        points = Answer.find_by(increment_id: value).scoring_points
        if points.has_key?("points")
          @experience_points += points["points"].to_i
        else
          puts points.inspect
        end
        puts "-------------------------------"
      end
    end
    @experience_points * @dynamic_coef
  end

  def maturity
    puts "---- Inside maturity ----"
    @maturity_points = 0
    fields = [:location_link, :visited_property_count, :search_time, :banker_contact, :broker_contact, :project_temporality]
    @dynamic_coef = 100.0 / (fields.size.to_f * 5.0)
    fields.each do |field|
      value = @survey[field.to_sym]
      if value
        if value.is_a?(Array)
          value.each do |val|
            points = Answer.find_by(increment_id: val).scoring_points
            if points.has_key?("points")
              @maturity_points += points["points"].to_i
            end
          end
        elsif value.is_a?(Integer)
          points = Answer.find_by(increment_id: value).scoring_points
          if points.has_key?("points")
            @maturity_points += points["points"].to_i
          end
        end
      end
    end
    @maturity_points * @dynamic_coef
  end

  def personality
    puts "---- Inside personality ----"
    @rational_max_pts = 142.0
    @affective_max_pts = 143.0
    @undecided_max_pts = 134.0
    @bold_max_pts = 126.0
    @hash_results = {}
    fields = [:house_works, :temperament_features, :couple_choice, :personality, :destination]
    @rational_pts = 0
    @affective_pts = 0
    @undecided_pts = 0
    @bold_pts = 0
    start_hash = {rational: 0, affective: 0, undecided: 0, bold: 0}
    fields.each do |field|
      value = @survey[field.to_sym]
      if value
        if value.is_a?(Array)
          puts "---- PERSONALITY ARRAY VALUE ----"
          value.each do |val|
            points = Answer.find_by(increment_id: val).scoring_points
            if points.has_key?("rational")
              @rational_pts += points["rational"]
              @affective_pts += points["affective"]
              @undecided_pts += points["undecided"]
              @bold_pts += points["bold"]
            end
          end
          puts "---------------------------------"
        elsif value.is_a?(Integer)
          puts "---- PERSONALITY INTEGER VALUE ----"
          puts value.inspect
          points = Answer.find_by(increment_id: value).scoring_points
          if points.has_key?("rational")
            @rational_pts += points["rational"]
            @affective_pts += points["affective"]
            @undecided_pts += points["undecided"]
            @bold_pts += points["bold"]
          end
          puts "-----------------------------------"
        end
      end
    end

    # Handle budget
    budget_result = handle_budget_scoring
    @rational_pts += budget_result[:rational]
    @affective_pts += budget_result[:affective]
    @undecided_pts += budget_result[:undecided]
    @bold_pts += budget_result[:bold]
    # End

    # Handle state
    state_result = handle_state_scoring
    @rational_pts += state_result[:rational]
    @affective_pts += state_result[:affective]
    @undecided_pts += state_result[:undecided]
    @bold_pts += state_result[:bold]
    # End

    start_hash[:rational] = @rational_pts
    start_hash[:affective] = @affective_pts
    start_hash[:undecided] = @undecided_pts
    start_hash[:bold] = @bold_pts
    start_hash
  end

  def flexibility
    puts "---- Inside flexibility ----"
    @flexibility_points = 0
    fields = [:school_type, :house_stairs, :flat_stairs, :dpe, :energy_renovation, :kitchen, :garage, :accessibility]
    @dynamic_coef = 100.0 / (fields.size.to_f * 5.0)
    fields.each do |field|
      value = @survey[field.to_sym]
      if value
        if value.is_a?(Array)
          value.each do |val|
            points = Answer.find_by(increment_id: val).scoring_points
            @flexibility_points += points["points"]
          end
        elsif value.is_a?(Integer)
          points = Answer.find_by(increment_id: value).scoring_points
          @flexibility_points += points["points"]
        end
      end
    end
    @flexibility_points * @dynamic_coef
  end

  def handle_budget_scoring
    sanitize_array = @survey.budget.reject{|item| item.empty?}
    if sanitize_array.size == 1
      start_hash = {rational: 5, affective: 2, undecided: 0, bold: 3}
    elsif sanitize_array.size == 2
      min_budget = sanitize_array.first.to_i
      max_budget = sanitize_array.last.to_i
      difference = max_budget - min_budget
      percent = ((difference.to_f * 100) / min_budget.to_f).round(0)
      if percent >= 0 and percent <= 5
        start_hash = {rational: 5, affective: 2, undecided: 0, bold: 3}
      elsif percent >= 6 and percent <= 12
        start_hash = {rational: 3, affective: 4, undecided: 3, bold: 5}
      elsif percent >= 13 and percent <= 20
        start_hash = {rational: 3, affective: 2, undecided: 4, bold: 5}
      elsif percent >= 21
        start_hash = {rational: 1, affective: 4, undecided: 5, bold: 2}
      end
    else
      start_hash = {rational: 0, affective: 0, undecided: 0, bold: 0}
    end
    start_hash ? start_hash : {rational: 0, affective: 0, undecided: 0, bold: 0}
  end

  def handle_state_scoring
    states = @survey[:state]
    if states.size == 1
      result = Answer.find_by(increment_id: states.last.to_i).text.downcase
      if result == "ancien"
        start_hash = {rational: 5, affective: 3, undecided: 1, bold: 5}
      elsif result == "neuf"
        start_hash = {rational: 5, affective: 1, undecided: 1, bold: 3}
      end
    else
      start_hash = {rational: 1, affective: 4, undecided: 5, bold: 1}
    end
    return start_hash
  end

end
