class ConditionTest
  prepend SimpleCommand

  def initialize(survey, question)
    @survey = survey
    @question = question
    @position = @question.position
    @results = []
    @next_question = Question.find_by(position: @position.to_i + 1)
  end

  def call
    next_question_logic
  end

  private

  def next_question_logic
    @next_question.conditions? ? handle_conditions : @next_question
  end

  def handle_conditions
    @next_question.conditions.size == 1 ? solo_condition : multi_conditions
  end

  def solo_condition
    if check_condition(@next_question.conditions.first)
      @next_question
    else
      @next_question = Question.find_by(position: @next_question.position.to_i + 1)
      next_question_logic
    end
  end

  def multi_conditions
    @logic = []
    @next_question.conditions.each do |condition|
      if condition.parent_operator
        if condition.parent_operator.to_sym == :and
          @logic << "&&"
        elsif condition.parent_operator.to_sym == :or
          @logic << "||"
        end
      end
      @logic << check_condition(condition)
    end
    puts "---- CONDITION LOGIC ----"
    puts @logic.inspect
    puts "-------------------------"
    if check_final_array(@logic)
      @next_question
    else
      @next_question = Question.find_by(position: @next_question.position.to_i + 1)
      next_question_logic
    end
  end

  def check_condition(condition)
    survey_value = @survey["#{condition.field_name.to_sym}"].to_i
    condition.send("#{condition.operator}_operator", survey_value)
  end

  def check_final_array(final_array)
    result = eval final_array.to_a.join
    puts "---- EVAL METHOD ----"
    puts result
    puts "---------------------"
    eval final_array.to_a.join
  end

end
