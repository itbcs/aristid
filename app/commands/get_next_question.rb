class GetNextQuestion
  prepend SimpleCommand

  def initialize(survey, question)
    @survey = survey
    @question = question
    @position = @question.position
    @results = []
  end

  def call
    next_question_logic
  end

  private

  def next_question_logic
    next_question = Question.find_by(position: @position.to_i + 1)
    next_question.conditions? ? next_conditionned_question(next_question) : next_unconditionned_question(next_question)
  end

  def next_unconditionned_question(next_question)
    next_question
  end

  def next_conditionned_question(next_question)
    @results = []
    next_question.conditions.sort_by{|condition| condition.order}.each do |condition|
      result = check_condition(condition)
      @results << result
      condition.parent_operator ? (@results << condition.parent_operator) : (puts "No child operator for this condition")
    end
    puts @results.inspect
    handle_results_v2(next_question)
    # handle_results(next_question)
  end

  def handle_results(next_question)
    puts "INSIDE HANDLE RESULTS METHOD"
    if @results.first == true
      return next_question
    else
      @potential_next_question = Question.find_by(position: next_question.position.to_i + 1)
      @potential_next_question.conditions? ? next_conditionned_question(@potential_next_question) : next_unconditionned_question(@potential_next_question)
    end
  end

  # Test
  def handle_results_v2(next_question)
    puts "INSIDE HANDLE RESULTS V2 METHOD"
    @results.size == 1 ? solo_result(next_question) : multi_result(next_question)
  end

  def solo_result(next_question)
    return next_question if @results.first == true
    @potential_next_question = Question.find_by(position: next_question.position.to_i + 1)
    @potential_next_question.conditions? ? next_conditionned_question(@potential_next_question) : next_unconditionned_question(@potential_next_question)
  end

  def multi_result(next_question)
    puts @results.inspect
    first_value = @results[0]
    operator = @results[-1]
    last_value = @results[1]
    if operator.to_sym == :or
      if or_operator(first_value,last_value)
        return next_question
      else
        @potential_next_question = Question.find_by(position: next_question.position.to_i + 1)
        @potential_next_question.conditions? ? next_conditionned_question(@potential_next_question) : next_unconditionned_question(@potential_next_question)
      end
    elsif operator.to_sym == :and
      if and_operator(first_value,last_value)
        return next_question
      else
        @potential_next_question = Question.find_by(position: next_question.position.to_i + 1)
        @potential_next_question.conditions? ? next_conditionned_question(@potential_next_question) : next_unconditionned_question(@potential_next_question)
      end
    end
  end
  # Fin test

  def check_condition(condition)
    survey_value = @survey["#{condition.field_name.to_sym}"].to_i
    result = condition.send("#{condition.operator}_operator", survey_value)
  end

  def and_operator(value1,value2)
    puts "---- inside and operator ----"
    puts "#{value1} #{value2}"
    value1 && value2
  end

  def or_operator(value1,value2)
    puts "---- inside or operator ----"
    puts "#{value1} #{value2}"
    value1 || value2
  end

end
