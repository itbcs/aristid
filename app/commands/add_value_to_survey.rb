class AddValueToSurvey
  prepend SimpleCommand
  include ActiveModel::Validations

  def initialize(survey, field, value)
    @survey = survey
    @field = field
    @value = value
  end

  def call
    @survey[@field.to_sym] = @value
    if @survey.save
      return @survey
    else
      errors.add(:base,:failure)
    end
  end

end
