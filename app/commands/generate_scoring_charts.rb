class GenerateScoringCharts
  prepend SimpleCommand
  include SvgHelper
  include ScoringGradeHelper

  def initialize(scoring)
    @scoring = scoring
  end

  def call
    # handle_charts
    handle_scoring_grades
  end

  private

  def handle_charts
    experience_chart = generate_svg(@scoring.experience.to_i,"rgb(232,120,88)")
    maturity_chart = generate_svg(@scoring.maturity.to_i,"rgb(120,134,200)")
    flexibility_chart = generate_svg(@scoring.flexibility.to_i,"rgb(120,134,200)")
    personality_chart = @scoring.personality
    result = {experience_chart: experience_chart, maturity_chart: maturity_chart, flexibility_chart: flexibility_chart}
    return result
  end

  def handle_scoring_grades
    experience_grade = generate_grade_scoring(@scoring.experience.to_i)
    maturity_grade = generate_grade_scoring(@scoring.maturity.to_i)
    flexibility_grade = generate_grade_scoring(@scoring.flexibility.to_i)
    personality_grade = generate_personality_scoring(@scoring.personality)
    results = {experience_grade: experience_grade, maturity_grade: maturity_grade, flexibility_grade: flexibility_grade, personality_grade: personality_grade}
    return results
  end

  def generate_svg(score,color)
    generate_half_pie_chart(fullcolor: "rgb(255,255,255)", color: color, percent: score, outerwidth: 17, innerwidth: 17)
  end

end
