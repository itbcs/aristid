class SurveyGeolocArea
  prepend SimpleCommand

  def initialize(geoloc_params)
    @params = geoloc_params
    @zipcodes = []
    @cities = []
    @fullresults = []
    @cities_zipcodes = []
    @errors = []
  end

  def call
    handle_areas
  end

  private

  def handle_areas
    @params.to_a.each do |placeid|
      response = RestClient.get("https://maps.googleapis.com/maps/api/geocode/json?place_id=#{placeid}&key=AIzaSyCdpP4BuVUKk75HM7NENKCUgo7jwFMwQwA")
      @result = (JSON.parse(response.body.to_s)) if response.code == 200
      @fullresults << @result.to_h
      address_component = @result["results"][0]["address_components"]
      geometry = @result["results"][0]["geometry"]
      @temp_hash = {}
      if geometry
        geometry.each do |hashed_values|
          if hashed_values.first == "location"
            @lat = hashed_values.last["lat"]
            @lng = hashed_values.last["lng"]
            @temp_hash[:lat] = @lat
            @temp_hash[:lng] = @lng
          end
        end
      end
      if address_component
        address_component.each do |hashed_values|
          if hashed_values["types"] == ["postal_code"]
            @zipcodes << hashed_values["short_name"]
            puts "---- Inside postal code block ----"
            puts hashed_values
            @temp_hash[:zipcode] = hashed_values["short_name"]
            puts @temp_hash.inspect
          elsif hashed_values["types"] == ["locality", "political"]
            @cities << hashed_values["short_name"]
            @temp_hash[:city] = hashed_values["short_name"]
          elsif hashed_values["types"] == ["administrative_area_level_1", "political"]
            if hashed_values["short_name"].to_s.downcase == "brittany"
              @temp_hash[:district] = "Bretagne"
            else
              @temp_hash[:district] = hashed_values["short_name"]
            end
          elsif hashed_values["types"] == ["administrative_area_level_2", "political"]
            @temp_hash[:department] = hashed_values["short_name"]
          elsif hashed_values["types"] == ["street_number"]
            @temp_hash[:street_number] = hashed_values["short_name"]
          elsif hashed_values["types"] == ["route"]
            @temp_hash[:street_name] = hashed_values["short_name"]
          end
        end
        @cities_zipcodes << @temp_hash
      end
      if !@temp_hash[:zipcode]
        if @temp_hash[:city]
          handle_result_without_zipcode_alt_api(@temp_hash[:city])
        else
          puts "There is no city in the google place result !!"
        end
      else
        puts "There is a zipcode in the response body"
      end
    end
    # puts "---- FULL RESULTS ----"
    # puts "Taille de la réponse: #{@fullresults.size}"
    # puts @fullresults
    # puts "----------------------"

    # puts "---- ZIPCODES ----"
    # puts @zipcodes.inspect
    # puts "------------------"

    puts "---- CITIES ----"
    puts @cities.inspect
    puts "----------------"

    # puts "---- CITIES PLUS ZIPCODES ----"
    # puts @cities_zipcodes.inspect
    # puts "------------------------------"

    # puts "---- UNIQ ZIPCODES ----"
    # puts @zipcodes.uniq.inspect
    # puts "------------------"
    areas = {zipcodes: @zipcodes.uniq, cities: @cities.uniq}
    @zipcodes.uniq
    return areas
  end

  def handle_result_without_zipcode(city)
    puts "---- Inside handle result without zipcode ----"
    response = RestClient.get("https://geo.api.gouv.fr/communes?nom=#{city.parameterize}")
    @result = (JSON.parse(response.body.to_s)) if response.code == 200
    @result.each do |item|
      if item["nom"].downcase == city.downcase
        puts "#{item['nom']} -> #{city.downcase}"
        item["codesPostaux"].each do |zipcode|
          @zipcodes << zipcode
        end
      end
    end
  end

  def handle_result_without_zipcode_alt_api(city)
    response = RestClient.get("https://api-adresse.data.gouv.fr/search/?q=#{city.parameterize}")
    @result = (JSON.parse(response.body.to_s)) if response.code == 200
    @result["features"].each do |item|
      if item["properties"]["city"].downcase.parameterize == city.downcase.parameterize
        zipcode = item["properties"]["postcode"]
        @zipcodes << zipcode
      end
    end
  end

end


