class FrontConditionsController < ApplicationController

  def new
    @question = Question.find(params[:question])
    @front_condition = FrontCondition.new()
  end

  def create
    @front_condition = FrontCondition.new(safe_params)
    if @front_condition.save
      redirect_to questions_path
    else
      render :new
    end
  end

  def edit
  end

  def update
  end

  def destroy
  end

  private

  def safe_params
    params.require(:front_condition).permit(:type,:min,:max,:survey_field,:question_id)
  end

end
