class ConditionsController < ApplicationController

  def show
    @condition = Condition.find(params[:id])
  end

  def index
    @conditions = Condition.all
  end

  def new
    @question = Question.find(params[:question])
    @condition = Condition.new()
    @conditions = @question.conditions
    if @conditions != []
      @available_order = (@question.conditions.sort_by{|condition| condition.order}.last.order.to_i) + 1
    end
  end

  def create
    @condition = Condition.new(safe_params)
    if @condition.save
      redirect_to questions_path
    else
      render :new
    end
  end

  def edit
  end

  def update
  end

  def destroy
  end

  def export_select_values
    options = ""
    values = []
    if params["field"]
      questions = Question.where(field_name: params["field"])
      questions.each do |question|
        question.answers.each do |answer|
          values << {label: "#{answer.text}", value: answer.increment_id}
        end
      end
    end
    values.each do |element|
      options += "<option value='#{element[:value].to_i}'>#{element[:label]}</option>"
    end
    full_html = ("<select class='select optional form-control' name='condition[value]' id='condition_value'>#{options}</select>").html_safe
    render json: {result: full_html}, status: :ok
  end

  private

  def safe_params
    params.require(:condition).permit(:question_id,:order,:operator,:field_name,:value,:parent_operator)
  end

end
