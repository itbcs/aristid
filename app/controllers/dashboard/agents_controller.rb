class Dashboard::AgentsController < ApplicationController
  respond_to :html

  def new
    @agent = current_agent
    @new_agent = Agent.new
    @agency = @agent.agency
  end

  def create
    puts params[:agent][:agent_role].inspect
    puts params[:agent][:agent_role].class
    @agency = current_agent.agency
    @agent = Agent.new(safe_params)
    if @agent.save
      puts "Success agent creation"
      redirect_to dashboard_agency_path
    else
      puts @agent.errors.full_messages
      @new_agent = @agent
      # render json: {errors: @agent.errors.full_messages}, status: :ok
      respond_with(@agent, location: "/dashboard/agents/new")
    end
  end

  private

  def safe_params
    params.require(:agent).permit(:email, :firstname, :lastname, :agent_role, :password, :password_confirmation, :agency_id)
  end

end
