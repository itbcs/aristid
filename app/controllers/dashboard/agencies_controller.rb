class Dashboard::AgenciesController < ApplicationController
  before_action :authenticate_agent!

  def start
    @agent = current_agent
    @agency = @agent.agency
  end

  def create_agent
    @agent = current_agent
    @agency = @agent.agency
  end

  def agents
    @agent = current_agent
    @agency = @agent.agency
    @agents = @agency.agents
  end

  def surveys
    @agent = current_agent
    @agency = @agent.agency
    @surveys = @agency.surveys
  end

  def survey
    @agent = current_agent
    @agency = @agent.agency
    @survey = Survey.find(params[:id])
    if @survey.survey_scoring
      @scoring = @survey.survey_scoring.personality
      values = {datas: [@scoring["undecided"], @scoring["bold"], @scoring["affective"], @scoring["rational"]]}
      generate_gon_variables(values)
    end
  end

  private

  def generate_gon_variables(hashed_values)
    gon.push(hashed_values)
  end

end
