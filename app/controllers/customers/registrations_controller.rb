class Customers::RegistrationsController < Devise::RegistrationsController
  skip_before_action :verify_authenticity_token, only: [:create]
  before_action :configure_sign_up_params, only: [:create]
  before_action :configure_account_update_params, only: [:update]
  layout false

  def new
    super
  end

  def create
    if params["survey_id"]
      puts "---- GOOD SURVEY ID IN PARAMS ----"
      puts params["survey_id"]
      puts "----------------------------------"
      @survey = Survey.find(params["survey_id"])
    end
    build_resource(sign_up_params)
    resource.save
    yield resource if block_given?
    if resource.persisted?
      puts "---- DEVISE REGISTRATIONS CONTROLLER ----"
      puts "Resource persisted !!"
      puts "-----------------------------------------"
      if resource.active_for_authentication?
        set_flash_message! :notice, :signed_up
        sign_up(resource_name, resource)
        # respond_with resource, location: after_sign_up_path_for(resource)
        puts "---- DEVISE REGISTRATIONS CONTROLLER ----"
        puts "Resource active for authentication !!"
        puts "-----------------------------------------"
        render json: {url: after_sign_up_path_for(resource)}
      else
        set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
        expire_data_after_sign_in!
        # respond_with resource, location: after_inactive_sign_up_path_for(resource)
        puts "---- DEVISE REGISTRATIONS CONTROLLER ----"
        puts "Resource not active for authentication !!"
        puts "-----------------------------------------"
        render :json => {connected: "Customer connected !!"}
      end
    else
      puts "---- DEVISE REGISTRATIONS CONTROLLER ----"
      puts "Resource not persisted !!"
      puts "-----------------------------------------"
      clean_up_passwords resource
      set_minimum_password_length
      # render :json => {errors: resource.errors}
      if session[:current_survey_id]
        @survey = Survey.find(session[:current_survey_id])
      end
      # render partial: "customers/registrations/new", resource: resource, layout: false
      respond_with(resource, location: "customers/registrations/new")
    end
  end

  def edit
    super
  end

  def update
    super
  end

  def destroy
    super
  end

  protected

  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute, :lastname, :firstname, :city, :country, :phone_number, :cgu, :promo_code, :survey_id])
  end

  def configure_account_update_params
    devise_parameter_sanitizer.permit(:account_update, keys: [:attribute, :lastname, :firstname, :city, :country, :phone_number, :cgu, :promo_code])
  end

  def after_sign_up_path_for(resource)
    resume_path(resource)
  end

  def after_update_path_for(resource)
    root_path
  end

end
