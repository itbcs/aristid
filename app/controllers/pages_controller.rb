class PagesController < ApplicationController

  def home
    files_content = File.read("#{Rails.root}/app/assets/images/svg/hand-pointer.svg")
    encoded_file = Base64.encode64(files_content)
    @cleaned_file = encoded_file.gsub("\n", "")
  end

  def scroll
  end

end
