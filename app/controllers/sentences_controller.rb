class SentencesController < ApplicationController

  def show
    @sentence = Sentence.find(params[:id])
  end

  def index
    @sentences = Sentence.all
  end

  def new
    @question = Question.find(params[:question])
    @sentence = Sentence.new
  end

  def create
    @sentence = Sentence.new(safe_params)
    if @sentence.save
      redirect_to questions_path
    else
      render :new
    end
  end

  def edit
    @sentence = Sentence.find(params[:id])
  end

  def update
  end

  def destroy
  end

  private

  def safe_params
    params.require(:sentence).permit(:text, :survey_field_to_display, :text_after_survey_field, :question_id)
  end

end
