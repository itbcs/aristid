class JourneyController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:start, :next_question_skip, :next_question]
  include ActionView::Helpers::AssetUrlHelper
  include SvgHelper

  def geoloc_start
    if params[:lname]
      @agency = Agency.find_by(name: params[:lname])
      if !@agency
        puts "This agency does not exsist !!!"
        redirect_to root_path, notice: "Attention cette agence n'exsiste pas !!"
      end
    end
    if current_customer
      puts "There is logged customer"
      sign_out current_customer
    else
      puts "There is no logged customer !!"
    end
    if current_agency
      puts "#{current_agency.email} is actually connected !!"
      sign_out current_agency
    else
      puts "There is no logged agency !!"
    end
    if current_agent
      puts "#{current_agent.email} is actually connected !!"
      sign_out current_agent
    else
      puts "There is no logged agent !!"
    end
    session[:current_survey_id] ? (session.delete(:current_survey_id)) : (puts "No session key")
  end

  def start
    if params[:agency]
      @agency = Agency.find(params[:agency])
      if @agency.custom_style?
        @primary_color = @agency.custom_style.primary_color
      end
    else
      @agency = Agency.find_by(name: "bcs-test")
    end
    if params["geoloc"]
      command = SurveyGeolocArea.call(params["geoloc"])
      if command.success?
        @zipcodes = command.result[:zipcodes]
        @cities = command.result[:cities]
        if @agency
          @survey = Survey.new(search_area: @zipcodes.to_a, cities: @cities.to_a, agency: @agency)
        else
          @survey = Survey.new(search_area: @zipcodes.to_a, cities: @cities.to_a)
        end
        if @survey.save
          session[:current_survey_id] = @survey.id.to_s
          puts "Survey successfully saved !!"
        else
          puts "An error occured when trying to save object !!"
        end
      else
        redirect_to root_path
      end
    else
      if params[:agency]
        @agency = Agency.find(params[:agency])
        if @agency.custom_style?
          @primary_color = @agency.custom_style.primary_color
        end
        redirect_to agency_start_page_path(@agency.name), notice: "Veuillez choisir une ville ou un code postal valide !!"
      else
        redirect_to root_path, notice: "Veuillez choisir une ville ou un code postal valide !!"
      end
    end
    @question = Question.find_by(position: 1)
    @answers_words_size = @question.answers_words_size
  end

  def next_question
    if params[:agency_id]
      @agency = Agency.find(params[:agency_id])
      if @agency.custom_style?
        @primary_color = @agency.custom_style.primary_color
      end
    else
      @agency = Agency.find_by(name: "bcs-test")
    end

    @survey = Survey.find(params[:survey_id])
    if params[:personality]
      @field_name = "personality"
      @field_value = handle_personality_params
    else
      @field_name = model_secure_params.keys.first
      @field_value = model_secure_params.values.first
    end

    command = AddValueToSurvey.call(@survey,@field_name,@field_value)
    if command.success?
      puts command.result
    else
      puts command.errors
    end

    if params[:question_position].to_i == Question.all.size
      render json: {end: "OK"}, status: :ok
    else
      @current_question = Question.find_by(position: (params[:question_position]))
      test_command = ConditionTest.call(@survey,@current_question)
      if test_command.success?
        puts test_command.result
        @question = test_command.result
      end
      @answers_words_size = @question.answers_words_size
      render "start", layout: false
    end
  end


  def next_question_skip
    if params[:agency_id]
      @agency = Agency.find(params[:agency_id])
      if @agency.custom_style?
        @primary_color = @agency.custom_style.primary_color
      end
    else
      @agency = Agency.find_by(name: "bcs-test")
    end

    @survey = Survey.find(params[:survey_id])
    @current_question = Question.find_by(position: (params[:question_position]))

    if params[:question_position].to_i == Question.all.size
      render json: {end: "OK"}, status: :ok
    else
      test_command = ConditionTest.call(@survey,@current_question)
      if test_command.success?
        puts test_command.result
        @question = test_command.result
      end
      @answers_words_size = @question.answers_words_size
      render "start", layout: false
    end
  end


  def resume
    @customer = Customer.find(params[:id])
    if session[:current_survey_id]
      @temp_survey = Survey.find(session[:current_survey_id])
      puts "---- Temporary survey inspector ----"
      puts @temp_survey.inspect
      puts "------------------------------------"
      @new_survey = @temp_survey.dup
      @new_survey.customer = @customer
      if @new_survey.save
        @temp_survey.destroy
        puts "survey cloned and deleted !!"
        session.delete(:current_survey_id)

        @agency = @customer.survey.agency

        command = SetSurveyScoring.call(@customer)
        if command.success?
          scoring_result = command.result
          charts_command = GenerateScoringCharts.call(scoring_result)
        else
          puts command.errors
          scoring_result = nil
        end

        sanitize_agency = @agency.as_json.to_h.except("_id")
        sanitize_customer = @customer.as_json.to_h.except("_id")
        sanitize_survey = @new_survey.as_json_with_slugs

        url = "https://bientotchezsoi.com/webhooks/survey"
        data = {agency: sanitize_agency, customer: sanitize_customer, survey: sanitize_survey, scoring: charts_command.result}.to_json
        content_type = {content_type: :json, accept: :json}
        RestClient.post(url,data,content_type)
      else
        puts "An error occured"
      end
    else
      @agency = @customer.survey.agency
      command = SetSurveyScoring.call(@customer)
      if command.success?
        puts "---- Current Scoring ----"
        puts command.result.inspect
        scoring_result = command.result
        charts_command = GenerateScoringCharts.call(scoring_result)
      else
        puts command.errors
      end
    end
  end

  private

  def model_secure_params
    Survey.automatic_safe_params(params)
  end

  def handle_personality_params
    formatted_params = []
    (1..8).each do |number|
      formatted_params << params.dig("personality_#{number}".to_sym)
    end
    puts formatted_params.inspect
    formatted_params.compact.map(&:to_i)
  end

end
