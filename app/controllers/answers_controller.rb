class AnswersController < ApplicationController

  def show
  end

  def index
    @questions = Question.all.sort_by{|question| question.position }
  end

  def new
    @question = Question.find(params[:question])
    @answers = @question.answers
    if @answers != []
      @available_order = (@question.answers.sort_by{|answer| answer.order}.last.order.to_i) + 1
    end
    @answer = Answer.new()
  end

  def create
    @question = Question.find(params[:answer][:question_id])
    @answer = Answer.new(safe_params)
    if @answer.save
      redirect_to questions_path, notice: "Bravo, réponse créee pour la question #{@question.position}"
    else
      render :new
    end
  end

  def edit
    @question = Question.find(params[:question])
    @answer = Answer.find(params[:id])
  end

  def update
    @answer = Answer.find(params[:id])
    puts @answer.inspect
    if params[:answer][:scoring_points]
      if params[:answer][:scoring_points].is_a?(String)
        puts "---- SCORING POINTS IS A STRING ----"
        @value = {points: params[:answer][:scoring_points].to_i}
      elsif params[:answer][:scoring_points].is_a?(ActionController::Parameters)
        puts "---- SCORING POINTS IS A ACTION CONTROLLER PARAMETERS ----"
        @value = {}
        params[:answer][:scoring_points].each do |k,v|
          @value[k.to_sym] = v.to_i
        end
      end
      @answer.scoring_points = @value
      puts @answer.inspect
      if @answer.save
        redirect_to answers_path
      else
        render :index
      end
    end
    # @question = Question.find(params[:answer][:question_id])
    # if @answer.update(safe_params)
    #   redirect_to answers_path
    # else
    #   render :index
    # end
  end

  def change_order
    @answer = Answer.find(params[:answer][:id])
    @question = @answer.question
    @answer.move_to! params[:answer][:new_order].to_i
    redirect_to question_path(@question)
  end

  def destroy
    @answer = Answer.find(params[:id])
    if @answer.destroy
      redirect_to question_path(@answer.question), notice: "Answer successfully destroy !!"
    else
      render :index
    end
  end

  private

  def safe_params
    params.require(:answer).permit(:text,:slug,:placeholder,:order,:question_id,:id,:new_order,scoring_points:[])
  end

end
