class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def not_found_redirect
    redirect_to not_found_path
  end

end
