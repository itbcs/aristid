class QuestionsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :find_question, only: [:show, :update, :destroy, :edit]

  def show
  end

  def index
    # handle_totals
    puts Survey.fields.except("_id","created_at","updated_at", "position").keys
    @questions = Question.all
  end

  def handle_totals
    points = []
    [:personality, :temperament_features, :house_works, :couple_choice].each do |field|
      points << Question.find_by(field_name: "#{field.to_s}").answers.map{|answer| answer.scoring_points}
    end
    start_hash = {rational: 0, affective: 0, undecided: 0, bold: 0}
    points.flatten.each do |points_hash|
      start_hash[:rational] += points_hash["rational"].to_i
      start_hash[:affective] += points_hash["affective"].to_i
      start_hash[:undecided] += points_hash["undecided"].to_i
      start_hash[:bold] += points_hash["bold"].to_i
    end
    puts "---- Handle Totals ----"
    puts start_hash.inspect
    puts "-----------------------"
  end

  def create
    @question = Question.new(safe_params)
    if @question.save
      redirect_to questions_path, notice: "Bravo, question créee"
    else
      render :new
    end
  end

  def new
    @last_question_position = Question.all.map{|question| question.position}.sort.last.to_i + 1
    puts "Last question position"
    puts @last_question_position
    @question = Question.new
  end

  def edit
  end

  def update
  end

  def destroy
  end

  def test
    puts ajax_safe_params.inspect
    render json: {test: "success"}.as_json, status: :ok
  end

  private

  def find_question
    @question = Question.find(params[:id])
  end

  def safe_params
    params.require(:question).permit(:position, :type, :required, :field_name, :label, :theme)
  end

  def ajax_safe_params
    params.except(:controller,:action).permit(:question, :type)
  end

end
