class Api::AgenciesController < ApplicationController

  def show
    if params[:email]
      email = params[:email].remove("<",">").strip
      puts "---- Email Inspector ----"
      puts email.inspect
      puts "-------------------------"
      @agency = Agency.find_by(aristid_email: email)
      if @agency
        puts "---- Inside aristid API Agency infos ----"
        puts @agency.inspect
        puts "-----------------------------------------"
        render json: {agency: @agency}, status: :ok
      else
        puts "Agency with email: #{email} does not exsist !!"
        render json: {error: "Agency with email: #{email}  does not exsist"}, status: 400
      end
    else
      render json: {error: "Email param must be inside your request"}, statut: 400
    end
  end

end
