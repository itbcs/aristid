class Agencies::RegistrationsController < Devise::RegistrationsController

  def new
  end

  def create
  end

  def edit
  end

  def update
  end

  def destroy
  end

  protected

  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute, :lastname, :firstname, :city, :country, :phone_number, :cgu, :promo_code])
  end

  def configure_account_update_params
    devise_parameter_sanitizer.permit(:account_update, keys: [:attribute, :lastname, :firstname, :city, :country, :phone_number, :cgu, :promo_code])
  end

  def after_sign_up_path_for(resource)
    resume_path(resource)
  end

  def after_update_path_for(resource)
    root_path
  end

end
