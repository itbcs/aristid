async function ajaxRequestConstructor(url,params,method) {
  try {
    const data = await postData(url, JSON.parse(params), method);
    console.log(data);
  } catch (error) {
    console.error(error);
  }
}

async function postData(url = '', data = {}, method) {
  const response = await fetch(url, {
    method: method,
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json'
    },
    redirect: 'follow',
    referrer: 'no-referrer',
    body: JSON.stringify(data)
  });
  console.log(response.status);
  return await response.json();
}

function handleAjaxrequest() {
  let trigger = document.querySelector(".ajax-trigger");
  if (trigger) {
    handleUnavailableDomObject(trigger);
  }
}

function handleUnavailableDomObject(trigger) {
  trigger.addEventListener("click", function(event) {
    let start = Date.now();
    let url = trigger.dataset.url;
    let params = trigger.dataset.params;
    let method = trigger.dataset.method;
    console.log(params);
    ajaxRequestConstructor(url,params,method);
    let end = Date.now();
    let time = end - start;
    console.log("seconds elapsed = " + (time/1000) + "s" + " Time = " + time + "ms");
  });
}

document.addEventListener("DOMContentLoaded",function(){
  handleAjaxrequest();
});
