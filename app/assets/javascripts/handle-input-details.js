document.addEventListener("DOMContentLoaded",function(){
  // handleInputDetails();
});


function handleInputDetails() {
  let triggers = document.querySelectorAll(".custom-radio-input input");
  let targets = document.querySelectorAll("aside");

  triggers.forEach((el) => {
    let index = el.dataset.index;
    el.addEventListener("click",function() {
      targets.forEach((el2) => {
        let targetIndex = el2.dataset.index;
        if (index == targetIndex) {
          console.log(index);
          console.log(targetIndex);
          el2.classList.remove("d-none");
        } else {
          el2.classList.add("d-none");
        }
      })
    });
  })
}
