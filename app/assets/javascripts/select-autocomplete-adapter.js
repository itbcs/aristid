$.fn.select2.amd.define('select2/data/googleAutocompleteAdapter', ['select2/data/array', 'select2/utils'],
    function (ArrayAdapter, Utils) {
        function GoogleAutocompleteDataAdapter ($element, options) {
            GoogleAutocompleteDataAdapter.__super__.constructor.call(this, $element, options);
        }
        Utils.Extend(GoogleAutocompleteDataAdapter, ArrayAdapter);
        GoogleAutocompleteDataAdapter.prototype.query = function (params, callback) {
            var returnSuggestions = function(predictions, status)
            {
                var data = {results: []};
                var zipcode;
                var zipcodes = [];
                var geocoder = new google.maps.Geocoder;
                if (status != google.maps.places.PlacesServiceStatus.OK) {
                  callback(data);
                }
                for(var i = 0; i< predictions.length; i++)
                {
                  data.results.push({id: predictions[i].place_id, text: predictions[i].description});
                }
                callback(data);
            };

            if(params.term && params.term != '')
            {
                var service = new google.maps.places.AutocompleteService();
                service.getPlacePredictions({ input: params.term, types: ['geocode'], componentRestrictions: {country: 'fr'} }, returnSuggestions);
            }
            else
            {
                var data = {results: []};
                callback(data);
            }
        };
        return GoogleAutocompleteDataAdapter;
    }
);
function formatRepo (repo) {
  if (repo.loading) {
    return repo.text;
  }
  var svgIcon = '<div class="geoloc-svg"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-map-pin"><path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path><circle cx="12" cy="10" r="3"></circle></svg></div>';
  var text = '<div class="repo-text">' + repo.text + '</div>';
  var markup = "<div class='select2-result-repository clearfix'>" +
      "<div class='select2-result-title'>" + svgIcon + text + "</div>";
  return markup;
}

function formatRepoSelection (repo) {
  return repo.text.replace(', France', "").substring(0,16);
}

var googleAutocompleteAdapter = $.fn.select2.amd.require('select2/data/googleAutocompleteAdapter');


function generateFrenchMessages() {
  return {
    errorLoading: function () {
      return 'Les résultats ne peuvent pas être chargés.';
    },
    inputTooLong: function (args) {
      var overChars = args.input.length - args.maximum;
      return 'Supprimez ' + overChars + ' caractère' +
        ((overChars > 1) ? 's' : '');
    },
    inputTooShort: function (args) {
      var remainingChars = args.minimum - args.input.length;
      return 'Saisissez au moins ' + remainingChars + ' caractère' +
        ((remainingChars > 1) ? 's' : '');
    },
    loadingMore: function () {
      return 'Chargement de résultats supplémentaires…';
    },
    maximumSelected: function (args) {
      return 'Vous pouvez seulement sélectionner ' + args.maximum +
        ' élément' + ((args.maximum > 1) ? 's' : '');
    },
    noResults: function () {
      return 'Aucun résultat trouvé';
    },
    searching: function () {
      return 'Recherche en cours…';
    },
    removeAllItems: function () {
      return 'Supprimer tous les éléments';
    }
  };
};


$(document).ready(function() {
  $('#advanced-input').select2({
    width: '100%',
    dataAdapter: googleAutocompleteAdapter,
    placeholder: 'Saisissez une ville, un code postal...',
    escapeMarkup: function (markup) { return markup; },
    minimumInputLength: 2,
    templateResult: formatRepo,
    dropdownParent: $('#select-custom-container'),
    language: generateFrenchMessages(),
    templateSelection: formatRepoSelection
  });
})





