function handleNestedQuestionResources() {
  let triggers = document.querySelectorAll(".form-add-nested-resource-instance");
  triggers.forEach((element) => {
    element.addEventListener("click",function(event) {
      let receiver = document.getElementById(element.dataset.receiver);
      let formToCopy = document.querySelector(element.dataset.formCopy);
      let formCopy = formToCopy.cloneNode(true);
      let inputs = formCopy.querySelectorAll("input");
      inputs.forEach((input) => {
        let name = input.getAttribute("name");
        console.log(name);
      })
      receiver.appendChild(formCopy);
    });
  })
}


document.addEventListener("DOMContentLoaded",function(){
  handleNestedQuestionResources();
});

