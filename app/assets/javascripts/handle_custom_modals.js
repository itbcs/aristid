function handleCustomModalsCloser() {
  $(".custom-modal-close-container").click(function(event) {
    console.log("Click on closer");
    $(".custom-modal-wrapper").addClass("d-none");
  });
}


$(document).ready(function() {
  handleCustomModalsCloser();
});
