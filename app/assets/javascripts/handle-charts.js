// $(document).ready(function(){
//   Chart = window.Chart;
//   Chart.defaults.RoundedDoughnut = Chart.helpers.clone(Chart.defaults.doughnut);
//   Chart.controllers.RoundedDoughnut = Chart.controllers.doughnut.extend({
//     draw: function (ease) {
//       var ctx = this.chart.chart.ctx;
//       var easingDecimal = ease || 1;
//       Chart.helpers.each(this.getMeta().data, function (arc, index) {
//         arc.transition(easingDecimal).draw();
//         var vm = arc._view;
//         var radius = (vm.outerRadius + vm.innerRadius) / 2;
//         var thickness = (vm.outerRadius - vm.innerRadius) / 2;
//         var angle = Math.PI - vm.endAngle - Math.PI / 2;
//         ctx.save();
//         ctx.fillStyle = vm.backgroundColor;
//         ctx.translate(vm.x, vm.y);
//         ctx.beginPath();
//         ctx.arc(radius * Math.sin(angle), radius * Math.cos(angle), thickness, 0, 2 * Math.PI);
//         ctx.arc(radius * Math.sin(Math.PI), radius * Math.cos(Math.PI), thickness, 0, 2 * Math.PI);
//         ctx.closePath();
//         ctx.fill();
//         ctx.restore();
//       });
//     },
//   });
// });


var options = {
  legend: {
    display: false
  },
  tooltips: {
    xPadding: 10,
    yPadding: 10,
    bodyFontSize: 16,
    bodyFontFamily: "-apple-system,BlinkMacSystemFont",
    footerFontFamily: "-apple-system,BlinkMacSystemFont"
  },
  plugins: {
    datalabels: {
      formatter: function(value, context) {
        return value + "%";
      },
      font: function(context) {
        var index = context.dataIndex;
        var value = context.dataset.data[index];
        var size = value / 4;
        return {
          size: size,
          weight: 700,
          family: "-apple-system,BlinkMacSystemFont"
        };
      },
      color: "rgb(250,250,250)"
    }
  },
  scale: {
    gridLines: {
      display: false
    },
    ticks: {
      display: false,
      suggestedMin: 5,
      suggestedMax: 100
    }
  }
};

var data = {
  labels: {
    defaultFontFamily: "-apple-system,BlinkMacSystemFont"
  },
  labels: ["Rationnel", "Affectif", "Indécis", "Audacieux"],
  datasets: [
    {
      backgroundColor: function(context) {
        var index = context.dataIndex;
        var value = context.dataset.data[index];
        if (value < 50) {
          return "rgba(231,120,87," + value/50 + ")";
        } else {
          return "rgba(80,100,180," + value/100 + ")";
        }
      },
      borderColor: "transparent",
      data: gon.datas
    }
  ]
};

$(document).ready(function() {
  console.log(gon.datas);
  var ctx = document.getElementById('personality').getContext('2d');
  new Chart(ctx, {
    type: 'polarArea',
    data: data,
    plugins: [window.ChartDataLabels],
    options: options
  });
});
