$(document).ready(function() {

  var scrolly = document.querySelector('#scrolly');
  var article = scrolly.querySelector('article');
  var step = article.querySelectorAll('.step');
  var scroller = scrollama();

  function handleProgress(response) {
    response.element.querySelector(".progress").textContent = Math.round(response.progress * 100) + " %";
    windowWidth = window.innerWidth;
    freeSpace = windowWidth - response.element.clientWidth;
    offset = (response.element.clientWidth * response.progress) - (response.element.clientWidth/4);
    realOffset = (freeSpace/2) * response.progress;
    response.element.style.transform = "translateX(" + realOffset + "px" + ")";
  }

  function handleStepEnter(response) {
    if (response.direction === "up") {
    } else if (response.direction === "down") {
    }
    response.element.classList.add('is-active');
  }

  function handleStepExit(response) {
    if (response.direction === "up") {
    } else if (response.direction === "down") {
    }
    response.element.classList.remove('is-active');
  }

  function init() {
    scroller.setup({
      step: '#scrolly article .step',
      debug: true,
      progress: true,
      offset: 0.5
    })
      .onStepEnter(handleStepEnter)
      .onStepProgress(handleProgress)
      .onStepExit(handleStepExit);
    window.addEventListener('resize', scroller.resize);
  }

  init();

});

