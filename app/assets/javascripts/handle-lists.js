$(document).ready(function() {
  var container = document.getElementById("aristid-agency-table");
  if (container !== null) {
    generateList();
  }
})

function generateList() {
  var options = {
    valueNames: [ 'date', 'name', 'surface', 'budget', 'type' ],
    pagination: true,
    page: 8
  };
  var demandList = new List("aristid-agency-table", options);
  console.log(demandList);
}
