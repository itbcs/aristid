function handleJourneySkipAjaxRequest() {
  $(".question-skip-btn").click(function(event) {
    event.preventDefault();
    var url = this.dataset.url;
    var position = this.dataset.position;
    var method = this.dataset.method;
    var survey = this.dataset.survey;
    var agency = this.dataset.agency;
    var exit = this.dataset.exit;
    var button = $(this);
    $.ajax({
      url: url,
      type: method,
      data: {question_position: position, survey_id: survey, agency_id: agency},
      beforeSend: function(event) {
        exitBlockLogic(exit,button);
        replaceSubmitButton();
      },
      success: function(data) {
        if (data.end) {
          console.log("End of journey");
          handleCustomerSignupLogic();
        } else {
          $("section.journey").remove();
          $("section.fixed-section").append($(data).find("section.journey"));
        }
      },
      error: function(data) {
        alert(data);
      },
      complete: function(data) {
        if (data.responseJSON) {
        } else {
          handleCompleteRequest();
        }
      }
    });
  });
}


function handleJourneyAjaxRequest() {
  $(".question-form").submit(function(event) {
    event.preventDefault();
    var $form = $(this);
    var url = $form.attr('action');
    var method = $form.attr('method');
    var button = $(".question-submit-btn");
    var exit = $(".question-submit-btn").data("exit");
    $.ajax({
      url: url,
      type: method,
      data: $form.serialize(),
      beforeSend: function(event) {
        exitBlockLogic(exit,button);
        replaceSubmitButton();
      },
      success: function(data) {
        if (data.end) {
          console.log("End of journey");
          handleCustomerSignupLogic();
        } else {
          $("section.journey").remove();
          $("section.fixed-section").append($(data).find("section.journey"));
        }
      },
      error: function(data) {
        alert(data);
      },
      complete: function(data) {
        if (data.responseJSON) {
        } else {
          handleCompleteRequest();
        }
      }
    });
  });
}


function exitAnimations() {
  var label = document.getElementById("questionlabel");
  var answer = document.getElementById("questionanswer");
  label.classList.add("exit-to-bottom");
  answer.classList.add("exit-to-bottom");
}

function enterAnimations() {
  var label = document.getElementById("questionlabel");
  var answer = document.getElementById("questionanswer");
  label.classList.add("enter-from-top");
  answer.classList.add("enter-from-top");
  label.addEventListener("animationend", function(event) {
    label.classList.remove("enter-from-top");
  },false);
  answer.addEventListener("animationend", function(event) {
    answer.classList.remove("enter-from-top");
  },false);
}


function exitBlockLogic(exit,domObject) {
  if (exit === "ON") {
    var exitIndex = $(domObject).data("exitindex");
    var section = document.querySelector("section.exitstep.exitstep-" + exitIndex.toString());
    section.classList.remove("d-none");
    section.classList.add("enter-from-bottom");
    section.addEventListener("animationend", function(event) {
      section.classList.remove("enter-from-bottom");
    },false);
    var button = section.querySelector(".exit-step-closer");
    button ? exitStepAction(button) : console.log("There is no button for this action");
  }
}

function exitStepAction(button) {
  var target = document.querySelector(button.dataset.target);
  button.addEventListener("click", function(event) {
    target.classList.add("exit-to-bottom");
    target.addEventListener("animationend", function(event) {
      target.classList.remove("exit-to-bottom");
      target.classList.add("d-none");
    });
  });
}

function replaceSubmitButton() {
  var element = document.querySelector("section.journey");
  var color = getComputedStyle(element).getPropertyValue("--agency-color");
  var svgLoader = '<div class="d-flex-cc mt-15px fade-green-color" style="color:' + color + ';"><svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946 s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634 c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"/><path fill="currentColor" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0 C22.32,8.481,24.301,9.057,26.013,10.047z"><animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 20 20" to="360 20 20" dur="0.8s" repeatCount="indefinite"/></path></svg></div>';
  $(".form-buttons-container").replaceWith($(svgLoader));
}


function handleCompleteRequest() {
  handleJourneyAjaxRequest();
  handleJourneySkipAjaxRequest();
  handleSubmitButtonsLogic();
  surveyRecapTrigger();
  handleCustomerSignupLogic();
  handleGeocodeInput();
  handleDeviseAjaxRequest();
  enterAnimations();
  handleCustomModalsCloser();
}



function handleGeocodeInput() {
  initializeAutocomplete('journey-geocode-input');
}


function handleSubmitButtonsLogic() {
  var submitButton = $(".question-submit-btn");
  var inputType = submitButton.data("type");
  if (inputType === "geocode") {
    var inputs = $(".question-answer input[type='text']");
  } else if (inputType === "textarea") {
    var inputs = $(".question-answer textarea");
  } else {
    var inputs = $(".question-answer input[type='" + inputType + "']");
  }
  if (inputType === "text" || inputType === "geocode" || inputType === "textarea") {
    TextInputButtonLogic(inputs,submitButton);
  } else if (inputType === "radio") {
    RadioInputButtonLogic(inputs,submitButton);
  } else if (inputType === "checkbox") {
    CheckboxInputButtonLogic(inputs,submitButton);
  } else if (inputType === "number") {
    NumberInputLogic(inputs,submitButton,inputType);
    NumberInputButtonLogic(submitButton);
  }
}

function NumberInputButtonLogic(submitButton) {
  submitButton.attr("disabled", false);
  submitButton.removeClass("disabled-btn");
}

function NumberInputLogic(inputs,submitButton,inputType) {
  var minusButton = $(".numeric-stepper-minus");
  var plusButton = $(".numeric-stepper-plus");
  var input = inputs.first();
  var minValue = parseInt(inputs.first().attr("min"));
  var maxValue = parseInt(inputs.first().attr("max"));
  minusButton.click(function() {
    var modifiedInputValue = $(".question-answer input[type='" + inputType + "']").val();
    if (parseInt(modifiedInputValue) === minValue) {
    } else {
      input.val(parseInt(modifiedInputValue) - 1);
    }
  });
  plusButton.click(function() {
    var modifiedInputValue = $(".question-answer input[type='" + inputType + "']").val();
    if (parseInt(modifiedInputValue) === maxValue) {
    } else {
      input.val(parseInt(modifiedInputValue) + 1)
    }
  })
}

function TextInputButtonLogic(inputs,submitButton) {
  if (inputs.first().data("condition") == "on") {
    return textFrontValidation(inputs,submitButton);
  }
  inputs.keyup(function() {
    if (this.value != "") {
      submitButton.attr("disabled", false);
      submitButton.removeClass("disabled-btn");
    } else {
      submitButton.attr("disabled", true);
      submitButton.addClass("disabled-btn");
    }
  });
}

function textFrontValidation(inputs,submitButton) {
  console.log("Inside textFrontValidation function !!!");
  console.log(inputs.first().data("min-value"));
  if (inputs.first().data("min-value") !== "" && inputs.first().data("min-value") !== undefined) {
    var min = parseInt(inputs.first().data("min-value"));
    inputs.keyup(function() {
      console.log("Input keyup with min value");
      var selection = window.getSelection().toString();
      if ( selection !== '' ) {
        return;
      }
      var input = $(this).val();
      var input = input.replace(/[\D\s\._\-]+/g, "");
      input = input ? parseInt( input, 10 ) : 0;
      $(this).val( function() {
        return ( input === 0 ) ? "" : input.toLocaleString( "fr-FR" );
      });
      if (input < min) {
        submitButton.attr("disabled", true);
        submitButton.addClass("disabled-btn");
      } else {
        submitButton.attr("disabled", false);
        submitButton.removeClass("disabled-btn");
      }
    });
  } else if (inputs.first().data("max-value") !== "") {
    console.log("Inside max value front condition");
    var max = parseInt(inputs.first().data("max-value"));
    console.log(max);
    inputs.keyup(function() {
      console.log("Max condition keyup !!!");
      var selection = window.getSelection().toString();
      if ( selection !== '' ) {
        return;
      }
      var input = $(this).val();
      var input = input.replace(/[\D\s\._\-]+/g, "");
      input = input ? parseInt( input, 10 ) : 0;
      $(this).val( function() {
        return ( input === 0 ) ? "" : input.toLocaleString( "fr-FR" );
      });
      if (input >= max) {
        submitButton.attr("disabled", true);
        submitButton.addClass("disabled-btn");
      } else {
        submitButton.attr("disabled", false);
        submitButton.removeClass("disabled-btn");
      }
    });
  }
}



function CheckboxInputButtonLogic(inputs,submitButton) {
  var checkedInputSize = 0;
  inputs.change(function(){
    if ($(this).prop("checked")) {
      checkedInputSize += 1
    } else {
      if (checkedInputSize != 0) {
        checkedInputSize -= 1;
      } else {
        checkedInputSize = 0;
      }
    }
    if (checkedInputSize != 0) {
      submitButton.attr("disabled", false);
      submitButton.removeClass("disabled-btn");
    } else {
      submitButton.attr("disabled", true);
      submitButton.addClass("disabled-btn");
    }
  });
}

function RadioInputButtonLogic(inputs,submitButton) {
  inputs.change(function(){
    if ($(this).prop("checked")) {
      submitButton.attr("disabled", false);
      submitButton.removeClass("disabled-btn");
    } else {
      submitButton.attr("disabled", true);
      submitButton.addClass("disabled-btn");
    }
  });
}

function surveyRecapTrigger() {
  $(".journey-survey-recap-trigger").click(function() {
    $("section.fixed_survey_recap_container").removeClass("d-none");
  });
  $(".survey-recap-close-trigger").click(function() {
    $("section.fixed_survey_recap_container").addClass("d-none");
  });
}



$(document).ready(function() {
  handleJourneyAjaxRequest();
  handleSubmitButtonsLogic();
  handleCustomerSignupLogic();
});
