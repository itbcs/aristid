function handleDeviseAjaxRequest() {
  $("#devise-customer-signup").submit(function(event) {
    event.preventDefault();
    var $form = $(this);
    var url = $form.attr('action');
    var method = $form.attr('method');
    replaceSignupSubmitButton();
    ajaxRequest($form,url,method);
  });
}

function replaceSignupSubmitButton() {
  var svgLoader = '<div class="d-flex-cc mt-15px fade-green-color"><svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946 s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634 c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"/><path fill="currentColor" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0 C22.32,8.481,24.301,9.057,26.013,10.047z"><animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 20 20" to="360 20 20" dur="0.8s" repeatCount="indefinite"/></path></svg></div>';
  $("#devise-customer-signup input[type='submit']").replaceWith($(svgLoader));
}

function ajaxRequest(form,url,method) {
  $.ajax({
    url: url,
    type: method,
    data: form.serialize(),
    beforeSend: function(event) {
    },
    success: function(data) {
      if (data.url) {
        window.location.href = data.url;
      } else {
        console.log(data);
        var receiver = $(".customer-signup-modal .custom-modal .custom-modal-body");
        receiver.empty();
        receiver.append($(data));
      }
    },
    error: function(data) {
    },
    complete: function(data) {
      handleDeviseAjaxRequest();
    }
  });
}


$(document).ready(function() {
  handleDeviseAjaxRequest();
});
