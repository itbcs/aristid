function handleDynamicSelectValues() {
  $("#new_condition select").change(function(event) {
    var fieldNameValue = this.value;
    var url = this.dataset.url;
    $.ajax({
      url: url,
      type: "GET",
      data: {field: fieldNameValue},
      beforeSend: function(event) {
      },
      success: function(data) {
        console.log(data);
        $(".ajax-values-container").empty();
        $(".ajax-values-container").append(data.result);
      },
      error: function(data) {
      },
      complete: function(data) {
      }
    });
  });
}


$(document).ready(function() {
  handleDynamicSelectValues();
});
