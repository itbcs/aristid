function handleCustomerSignupLogic() {
  $(".signup-trigger").click(function() {
    var target = this.dataset.target;
    $(target).removeClass("d-none");
  });
}



$(document).ready(function() {
  handleCustomerSignupLogic();
})
