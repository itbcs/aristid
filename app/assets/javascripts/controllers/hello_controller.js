(() => {
  stimulus.register("hello", class extends Stimulus.Controller {
    static get targets() {
      return ["output","name"]
    }
    greet() {
      this.outputTarget.textContent = `Hello, ${this.nameTarget.value}!`
    }
  })
})()
