function handleAgentCreate() {
  $("form.new_agent").submit(function(event) {
    console.log(this);
    event.preventDefault();
    var $form = $(this);
    var url = $form.attr('action');
    var method = $form.attr('method');
    $.ajax({
      url: url,
      type: method,
      data: $form.serialize(),
      beforeSend: function(event) {
      },
      success: function(data) {
        console.log(data);
      },
      error: function(data) {
      },
      complete: function(data) {
      }
    });
  });
}


$(document).ready(function() {
  // handleAgentCreate();
});
