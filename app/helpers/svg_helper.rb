module SvgHelper

  def polar_to_cartesian(center_x,center_y,radius,angle_in_degrees)
    multiplier = Math::PI / 180
    angle_in_radian = ((angle_in_degrees.to_i - 180) * Math::PI) / 180
    r = {x: (center_x + (radius * Math.cos(angle_in_radian))), y: (center_y + (radius * Math.sin(angle_in_radian)))}
    return r
  end

  def describe_arc(x,y,radius,start_angle,end_angle)
    start_path = polar_to_cartesian(x,y,radius,end_angle)
    end_path = polar_to_cartesian(x,y,radius,start_angle)
    largeArcFlag = (end_angle - start_angle <= 180) ? "0" : "1";
    d = ["M",start_path[:x],start_path[:y],"A",radius,radius,0,largeArcFlag,0,end_path[:x],end_path[:y]].join(" ")
    return d
  end

  def percent_to_radian(percent)
    r = (percent * 180.0) / 100.0
    return r.to_i
  end

  def generate_half_pie_chart(args={})
    return nil if !args[:percent]
    percent = args[:percent]
    fullcolor = args[:fullcolor] ? args[:fullcolor] : "rgb(230,230,230)"
    outerwidth = args[:outerwidth] ? args[:outerwidth] : 17
    innerwidth = args[:innerwidth] ? args[:innerwidth] : 7
    color = args[:color] ? args[:color] : "rgb(50,160,220)"
    text_color = "rgb(50,50,50)"

    left_line = generate_line(87,152,92,152,"round","round",1)
    left_text = generate_text(76,155,"-apple-system,BlinkMacSystemFont",text_color,0,9,600)
    left_line_group = "<g stroke='#{fullcolor}'>#{left_line}#{left_text}</g>"

    mid_line = generate_line(150,86,150,91,"round","round",1)
    mid_text = generate_text(144,80,"-apple-system,BlinkMacSystemFont",text_color,50,9,600)
    mid_line_group = "<g stroke='#{fullcolor}'>#{mid_line}#{mid_text}</g>"

    right_line = generate_line(208,152,213,152,"round","round",1)
    right_text = generate_text(217,155,"-apple-system,BlinkMacSystemFont",text_color,100,9,600)
    right_line_group = "<g stroke='#{fullcolor}'>#{right_line}#{right_text}</g>"

    big_value = generate_text_with_anchor(150,155,"-apple-system,BlinkMacSystemFont",text_color,percent,18,700)

    fullcircle_path = "<path fill='none' stroke='#{fullcolor}' stroke-linecap='round' stroke-linejoin='round' stroke-width='#{outerwidth}' d='#{describe_arc(150, 150, 50, 0, 180)}'></path>"
    circle_path = "<path fill='none' stroke='#{color}' stroke-linecap='round' stroke-linejoin='round' stroke-width='#{innerwidth}' d='#{describe_arc(150, 150, 50, 0, percent_to_radian(percent))}'></path>"
    ticks = "<div class='min-tick'><div class='tick-value'>0</div><div class='tick'></div></div><div class='middle-tick'><div class='tick-value'>50</div><div class='tick'></div></div><div class='max-tick'><div class='tick'></div><div class='tick-value'>100</div></div>"
    svg = "<svg preserveAspectRatio='xMidYMid slice' viewbox='0 0 300 250'>" + fullcircle_path + circle_path + left_line_group + mid_line_group + right_line_group + big_value + "</svg>"
    value_display = "<div class='value-display' style='color: #{color};'>#{percent}</div>"
    full_container = ("<div class='halfpie-chart-container' style='color: #{fullcolor};'>" + svg + "</div>").html_safe
    return full_container
  end

  def generate_line(x1,y1,x2,y2,linecap,linejoin,stroke_width)
    "<line x1='#{x1}' y1='#{y1}' x2='#{x2}' y2='#{y2}' stroke-width='#{stroke_width}' stroke-linejoin='#{linejoin}' stroke-linecap='#{linecap}'/>"
  end

  def generate_text(x,y,font_family,fill_color,text,font_size,font_weight)
    "<text x='#{x}' y='#{y}' style='font-family: #{font_family};font-size: #{font_size};font-weight: #{font_weight};stroke-width: 0;stroke: transparent;fill: #{fill_color};'>#{text}</text>"
  end

  def generate_text_with_anchor(x,y,font_family,fill_color,text,font_size,font_weight)
    "<text text-anchor='middle' x='#{x}' y='#{y}' style='font-family: #{font_family};font-size: #{font_size};font-weight: #{font_weight};stroke-width: 0;stroke: transparent;fill: #{fill_color};'>#{text}</text>"
  end

  def generate_rounded_progress_bar(args={})
    return nil if !args[:percent]
    height = args[:height] ? "#{args[:height]}px" : "25px"
    fullcolor = args[:fullcolor] ? args[:fullcolor] : "rgb(241,242,247)"
    color = args[:color] ? args[:color] : "rgb(50,160,220)"
    percent = "#{args[:percent]}%"
    innerbar = "<div class='inner-progress-bar' style='width: #{percent};background-color: #{color};'>#{percent}</div>"
    full_container = "<div class='rounded-progress-bar' style='background-color: #{fullcolor};height: #{height};'>" + innerbar + "</div>"
    left_text = "<span class='progress-label-text'>Faible</span>"
    right_text = "<span class='progress-label-text'>Forte</span>"
    full_container_with_labels = ("<div class='progress-fullcontainer'>" + left_text + full_container + right_text + "</div>").html_safe
    return full_container_with_labels
  end

end
