module SurveyHelper
  include ActionView::Helpers::NumberHelper

  def format_budget(budget_array)
    clone_object = budget_array.dup.reject{|item| item.empty?}.map{|item| "#{number_with_delimiter(item.to_i, delimiter: ' ')} €"}
    if clone_object.size == 1
      result = clone_object.first
    elsif clone_object.size == 2
      result = "Entre #{clone_object.join('et')}"
    end
    return result
  end

  def format_phone(phone)
    phone.phony_formatted(normalize: :FR, spaces: ' ')
  end

end
