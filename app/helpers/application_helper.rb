module ApplicationHelper

  def svg(name)
    file_path = "#{Rails.root}/app/assets/images/svg/#{name}.svg"
    return File.read(file_path).html_safe if File.exists?(file_path)
    '(not found)'
  end

  def feather_svg(name)
    file_path = "#{Rails.root}/app/assets/images/svg/feather/#{name}.svg"
    return File.read(file_path).html_safe if File.exists?(file_path)
    '(not found)'
  end

  def remote_link_to(*args)
    text = args[0]
    url = args[1]
    html_class = args[2][:class]
    html_params = args[2][:params].to_json
    html = ("<div class='#{html_class}' data-url='#{url}' data-method='POST' data-params='#{html_params}'>#{text}</div>").html_safe
    return html
  end

end
