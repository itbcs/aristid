module ScoringGradeHelper

  def generate_grade_scoring(percent)
    content = []
    if percent >= 0 and percent < 17
      active_item = "f"
    elsif percent >= 17 and percent < 34
      active_item = "e"
    elsif percent >= 34 and percent < 50
      active_item = "d"
    elsif percent >= 50 and percent < 67
      active_item = "c"
    elsif percent >= 67 and percent < 84
      active_item = "b"
    elsif percent >= 84
      active_item = "a"
    end
    ("a".."f").to_a.each do |letter|
      content << "<div class='scoring-item #{letter}-grade #{'active' if letter == active_item}'>#{letter.capitalize}</div>"
    end
    style = "-webkit-box-align: center;display: -webkit-box;display: box;"
    container = "<div class='grade-scoring-container' style='#{style}'>#{content.join()}</div>"
    container.html_safe
  end

  def generate_personality_scoring(score)
    rational = score[:rational].to_i
    affective = score[:affective].to_i
    undecided = score[:undecided].to_i
    bold = score[:bold].to_i
    left_block_content = []
    right_block_content = []
    item_style = "display: -webkit-box;display: box;-webkit-box-pack: center;-webkit-box-orient: vertical;box-orient: vertical;-webkit-box-align: center;"

    bold_title = "<div class='personality-title'>Audacieux</div>"
    bold_value = "<div class='personality-percent'>#{bold}</div>"
    undecided_title = "<div class='personality-title'>Indécis</div>"
    undecided_value = "<div class='personality-percent'>#{undecided}</div>"

    affective_title = "<div class='personality-title'>Affectif</div>"
    affective_value = "<div class='personality-percent'>#{affective}</div>"
    rational_title = "<div class='personality-title'>Rationnel</div>"
    rational_value = "<div class='personality-percent'>#{rational}</div>"

    if bold > undecided
      bold_item = "<div class='personality-item personality-red' style='#{item_style}'>#{bold_title}#{bold_value}</div>"
      undecided_item = "<div class='personality-item personality-light-blue' style='#{item_style}'>#{undecided_title}#{undecided_value}</div>"
      left_block_content << bold_item
      right_block_content << undecided_item
    else
      bold_item = "<div class='personality-item personality-light-red' style='#{item_style}'>#{bold_title}#{bold_value}</div>"
      undecided_item = "<div class='personality-item personality-blue' style='#{item_style}'>#{undecided_title}#{undecided_value}</div>"
      left_block_content << bold_item
      right_block_content << undecided_item
    end

    if affective > rational
      affective_item = "<div class='personality-item personality-red' style='#{item_style}'>#{affective_title}#{affective_value}</div>"
      rational_item = "<div class='personality-item personality-light-blue' style='#{item_style}'>#{rational_title}#{rational_value}</div>"
      left_block_content << rational_item
      right_block_content << affective_item
    else
      affective_item = "<div class='personality-item personality-light-red' style='#{item_style}'>#{affective_title}#{affective_value}</div>"
      rational_item = "<div class='personality-item personality-blue' style='#{item_style}'>#{rational_title}#{rational_value}</div>"
      left_block_content << rational_item
      right_block_content << affective_item
    end

    left_block = "<div class='personality-left-block'>#{left_block_content.join()}</div>"
    right_block = "<div class='personality-right-block'>#{right_block_content.join()}</div>"

    container_style = "display: -webkit-box;display: box;"
    container = "<div class='personality-pdf-container' style='#{container_style}'>#{left_block}#{right_block}</div>"
    return container.html_safe
  end

end
