class Answer
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Orderable
  include GlobalID::Identification

  belongs_to :question
  before_save :nil_if_blank
  orderable scope: :question, column: :order
  auto_increment :increment_id, collection: :answer_increment_sequences

  field :text, type: String
  field :slug, type: String, default: nil
  field :order, type: Integer
  field :placeholder, type: String
  field :min, type: Integer
  field :max, type: Integer
  field :scoring_points, type: Hash, default: {}

  def self.sanitize_fields_for_meta_prog
    fields_to_ignore = ["_id","created_at","updated_at","increment_id","question_id"]
    @instance_fields = []
    Answer.fields.each do |field|
      if !fields_to_ignore.include?(field.last.name)
        @instance_fields << field.last.name
      end
    end
    @instance_fields
  end

  private

  def nil_if_blank
    puts self.inspect
    Answer.sanitize_fields_for_meta_prog.each { |attr| self[attr] = nil if self[attr].blank? }
  end

end
