class CustomStyle
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :agency

  field :primary_color, type: String
  field :secondary_color, type: String
  field :opening_sentence, type: String
  field :male_avatar, type: String
  field :female_avatar, type: String
  field :undefined_avatar, type: String
  field :logo, type: String
  field :dark_logo, type: String
  field :fullname, type:  String

end
