class Sentence
  include Mongoid::Document
  include Mongoid::Timestamps
  belongs_to :question

  field :text, type: String
  field :survey_field_to_display, type: String, default: nil
  field :text_after_survey_field, type: String, default: nil

end
