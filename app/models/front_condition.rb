class FrontCondition
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :question
  before_save :add_question_answers_size
  before_save :handle_survey_field_param

  field :min, type: Integer
  field :max, type: Integer
  field :survey_field, type: String, default: nil
  field :answers_size, type: Integer

  def add_question_answers_size
    self.answers_size = self.question.answers.size.to_i
  end

  def handle_survey_field_param
    (self[:survey_field] == "") ? (self.survey_field = nil) : (self.survey_field = self[:survey_field])
  end

end
