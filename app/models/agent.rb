class Agent
  include Mongoid::Document
  include Mongoid::Timestamps
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable
  belongs_to :agency
  belongs_to :agent_role

  validates :firstname, presence: true
  validates :lastname, presence: true
  validates :agent_role, presence: true

  # Devise field
  field :email, type: String, default: ""
  field :encrypted_password, type: String, default: ""
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time
  field :remember_created_at, type: Time
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  field :firstname, type: String
  field :lastname, type: String
  field :phone, type: String
  field :active, type: Boolean, default: false
  field :avatar, type: String

  def is_admin?
    if self.agent_role
      self.agent_role.name.to_sym == :admin ? true : false
    else
      nil
    end
  end

  def is_agent?
    if self.agent_role
      self.agent_role.name.to_sym == :agent ? true : false
    else
      nil
    end
  end

end
