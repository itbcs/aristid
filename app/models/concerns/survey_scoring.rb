class SurveyScoring
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :survey, optional: true

  field :experience, type: Integer
  field :personality, type: Hash
  field :maturity, type: Integer
  field :flexibility, type: Integer

  def self.theme
    return [:experience, :maturity, :personality, :flexibility]
  end

end
