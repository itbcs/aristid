class ConditionType

  def self.list_with_datas
    return [:superior, :inferior, :equal, :not_equal]
  end

  def self.parent_operator_list
    return [:or, :and]
  end

end
