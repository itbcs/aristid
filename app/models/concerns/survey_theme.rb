class SurveyTheme
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :details, type: String

  def self.list
    return [:personality, :project, :context, :history]
  end

  def self.list_with_datas
    datas = []
    SurveyTheme.all.each do |surveytheme|
      datas << {name: surveytheme.name.to_sym, details: surveytheme.details}
    end
    return datas
  end


end
