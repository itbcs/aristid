class AgentRole
  include Mongoid::Document
  include Mongoid::Timestamps
  has_many :agents

  field :name, type: String
  field :access, type: Array

  def self.list
    @datas = []
    AgentRole.all.each do |ar|
      @datas << {name: ar.name, id: ar.id.to_s}
    end
    return @datas
  end

  def self.form_list
    @datas = []
    AgentRole.all.each do |ar|
      @datas << [ar.name, "#{ar.id}"]
    end
    @datas
  end

  def self.ids_array
    @datas = []
    AgentRole.all.each do |ar|
      @datas << ar.id
    end
    @datas.to_a
  end

end
