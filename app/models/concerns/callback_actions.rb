module CallbackActions
  extend ActiveSupport::Concern

  included do
    self.fields.each do |field|
      method_name = field.last.name
      field_name = method_name
      full_method_name = "#{method_name}_format_params"
      field_type = field.last.type
      if field_type == Array
        if field_name.to_sym == :budget
          define_method("#{full_method_name}".to_sym) do
            self["#{field_name}".to_sym] ? (self["#{field_name}".to_sym] = self["#{field_name}".to_sym].map!{|item| item.gsub(/[[:space:]]/,'')}) : (puts "No #{field_name} params")
          end
          set_callback :save, :before, full_method_name.to_sym
        elsif field_name.to_sym == :search_area
        elsif field_name.to_sym == :cities
        else
          define_method("#{full_method_name}".to_sym) do
            self["#{field_name}".to_sym] ? (self["#{field_name}".to_sym] = self["#{field_name}".to_sym].map!(&:to_i)) : (puts "No #{field_name} params")
          end
          set_callback :save, :before, full_method_name.to_sym
        end
      end
    end
  end

end
