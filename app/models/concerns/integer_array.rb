class IntegerArray
  attr_reader :values

  def initialize(values)
    @values = values
  end

  def mongoize
    @values.map!(&:to_i)
  end

  class << self

    def demongoize(object)
      return self.new(object.map!(&:to_i)).values if object.is_a?(Array)
    end

    def mongoize(object)
      case object
        when IntegerArray then object.mongoize
        when Array then IntegerArray.new(object.map!(&:to_i)).mongoize
        else object
      end
    end

    def evolve(object)
      case object
        when IntegerArray then object.mongoize
        else object
      end
    end

  end

end
