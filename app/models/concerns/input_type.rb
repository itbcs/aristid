class InputType
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :details, type: String

  def self.list
    return [:text, :checkbox, :radio, :number, :date]
  end

  def self.list_with_datas
    datas = []
    InputType.all.each do |input_type|
      datas << {name: input_type.name.to_sym, details: input_type.details}
    end
    return datas
  end

end
