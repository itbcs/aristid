module SurveyMethods
  extend ActiveSupport::Concern
      included do

        def self.meta_prog_fields
          fields_to_ignore = ["_id","search_area","cities","created_at","updated_at","customer_id","agency_id","firstname","rooms","surface","budget","principal_space","current_address","actual_housing_area","last_precision"]
          @instance_fields = []
          self.fields.each do |field|
            if !fields_to_ignore.include?(field.last.name)
              @instance_fields << {field_type: field.last.type, field_name: field.last.name}
            end
          end
          @instance_fields
        end

        def self.form_fields
          [:utf8,:authenticity_token,:action,:controller,:survey_id,:question_position]
        end

        def self.automatic_safe_params(params)
          @result = []
          self.fields.each do |field|
            type = field.last.type
            name = field.last.name
            if type == Integer || type == String
              @result << name.to_sym
            elsif type == Array
              container = {}
              container[name.to_sym] = []
              @result << container
            end
          end
          self.form_fields.each do |field|
            @result << field
          end
          params.permit(@result).except(:utf8,:authenticity_token,:action,:controller,:survey_id,:question_position,:agency_id)
        end

        self.meta_prog_fields.each do |field|
          method_name = field[:field_name]
          field_name = method_name
          field_type = field[:field_type]
          if field[:field_type] == Array
            define_method("#{field[:field_name]}".to_sym) do
              self["#{field[:field_name]}".to_sym] ? self["#{field[:field_name]}".to_sym].map{|identifier| Answer.find_by(increment_id: identifier).text } : nil
            end
            define_method("#{field[:field_name]}_slug".to_sym) do
              self["#{field[:field_name]}".to_sym] ? self["#{field[:field_name]}".to_sym].map{|identifier| Answer.find_by(increment_id: identifier).slug } : nil
            end
          elsif field[:field_type] == Integer
            define_method("#{field[:field_name]}".to_sym) do
              self["#{field[:field_name]}".to_sym] ? Answer.find_by(increment_id: self["#{field[:field_name]}".to_sym]).text : nil
            end
            define_method("#{field[:field_name]}_slug".to_sym) do
              self["#{field[:field_name]}".to_sym] ? Answer.find_by(increment_id: self["#{field[:field_name]}".to_sym]).slug : nil
            end
          end
        end

      end


end
