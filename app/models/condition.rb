class Condition
  include Mongoid::Document
  include Mongoid::Timestamps
  belongs_to :question

  field :order, type: Integer
  field :operator, type: String
  field :field_name, type: String
  field :value, type: Integer
  field :has_child, type: Boolean
  field :child_operator, type: String
  field :parent_operator, type: String

  def not_equal_operator survey_value
    survey_value != self.value
  end

  def equal_operator survey_value
    puts "Survey_value -> #{survey_value}"
    puts "Self.value -> #{self.value}"
    survey_value == self.value
  end

  def and_operator survey_value
    survey_value && self.value
  end

  def or_operator survey_value
    survey_value || self.value
  end

  def superior_operator survey_value
    survey_value > self.value
  end

  def inferior_operator survey_value
    survey_value < self.value
  end

end
