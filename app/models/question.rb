class Question
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Orderable
  include GlobalID::Identification
  orderable
  has_many :answers, dependent: :destroy
  has_many :conditions, dependent: :destroy
  has_one :sentence, dependent: :destroy
  has_one :front_condition, dependent: :destroy

  field :position, type: Integer
  field :type, type: String
  field :required, type: Boolean
  field :field_name, type: String
  field :label, type: String
  field :theme, type: String
  field :hint, type: String, default: nil
  field :js_validation, type: Boolean, default: false
  field :js_validation_min_value, type: Integer, default: nil
  field :exit_step, type: Boolean
  field :exit_index, type: Integer
  field :radio_by_pair, type: Boolean
  field :field_in_label_to_display, type: String
  field :text_to_display_after_variable, type: String, default: nil
  field :specific_display, type: Boolean, default: nil

  def answers_words_size
    self.answers.map{|answer| answer.text}.compact.map{|text| text.chars.size}.compact.sum
  end

end
