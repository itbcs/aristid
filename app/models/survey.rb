class Survey
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  belongs_to :customer, optional: true
  belongs_to :agency, optional: true
  has_one :survey_scoring, dependent: :destroy

  field :gender, type: Integer
  field :firstname, type: String
  field :property_type, type: Integer
  field :destination, type: Integer
  field :project, type: Integer
  field :rooms, type: Integer
  field :surface, type: String
  field :state, type: Array
  field :budget, type: Array
  field :actual_situation, type: Integer
  field :current_location, type: Integer
  field :location_link, type: Array
  field :project_temporality, type: Integer
  field :visited_property_count, type: Integer
  field :house_works, type: Integer
  field :school_type, type: Array
  field :house_stairs, type: Integer
  field :flat_stairs, type: Integer
  field :principal_space, type: String
  field :man_persona, type: Integer
  field :woman_persona, type: Integer
  field :personality, type: Array
  field :age, type: Integer
  field :current_address, type: String
  field :actual_housing, type: Integer
  field :installation_date, type: Integer
  field :actual_housing_area, type: String
  field :actual_housing_strong_points, type: String
  field :search_time, type: Integer
  field :dpe, type: Array
  field :energy_renovation, type: Integer
  field :couple_choice, type: Integer
  field :style, type: Array
  field :kitchen, type: Integer
  field :accessibility, type: Integer
  field :parking, type: Array
  field :garage, type: Integer
  field :house_exterior, type: Array
  field :flat_exterior, type: Array
  field :proximity_situation, type: Integer
  field :favorite_transportation_mode, type: Array
  field :banker_contact, type: Integer
  field :broker_contact, type: Integer
  field :temperament_features, type: Array
  field :last_precision, type: String
  field :search_area, type: Array
  field :cities, type: Array

  include SurveyMethods
  include CallbackActions

  def is_owner?
    if self.actual_situation
      if self.actual_situation.include?("propriétaire")
        value = true
      else
        value = false
      end
    else
      value = false
    end
    return value
  end

  def self.sanitize_fields
    Survey.fields.except("_id","created_at","updated_at", "position").keys
  end

  def french_instance
    new_instance = {}
    fields_to_ignore = ["firstname","rooms","principal_space","last_precision","actual_housing_area","search_area","budget","surface","actual_housing_strong_points","current_address","cities","agency_id","customer_id"]
    self.attributes.except("_id","created_at","updated_at").each do |k,v|
      if fields_to_ignore.include?(k.to_s)
        new_instance[Survey.human_attribute_name("#{k}")] = v
      else
        if v.is_a?(Array)
          array_values = []
          v.each do |val|
            array_values << Answer.find_by(increment_id: val.to_i).slug
          end
          new_instance[Survey.human_attribute_name("#{k}")] = array_values
        else
          new_instance[Survey.human_attribute_name("#{k}")] = Answer.find_by(increment_id: v.to_i).slug
        end
      end
    end
    new_instance
  end

  def as_json_with_slugs
    fields_to_ignore = ["_id","created_at","updated_at","customer_id"]
    @result = {}
    self.fields.each do |field|
      if !fields_to_ignore.include?(field.last.name)
        if field.last.type == Array
          if field.last.name == "search_area"
            @result["#{field.last.name}".to_sym] = self.send("#{field.last.name}")
          elsif field.last.name == "cities"
            @result["#{field.last.name}".to_sym] = self.send("#{field.last.name}")
          elsif field.last.name == "budget"
            @result["#{field.last.name}".to_sym] = self.send("#{field.last.name}")
          elsif field.last.name == "surface"
            @result["#{field.last.name}".to_sym] = self.send("#{field.last.name}")
          else
            @result["#{field.last.name}".to_sym] = self.send("#{field.last.name}_slug")
          end
        elsif field.last.type == Integer
          if field.last.name != "rooms"
            @result["#{field.last.name}".to_sym] = self.send("#{field.last.name}_slug")
          else
            @result["#{field.last.name}".to_sym] = self.send("#{field.last.name}")
          end
        elsif field.last.type == String
          @result["#{field.last.name}".to_sym] = self.send("#{field.last.name}")
        end
      end
    end
    @result
  end

  def boy?
    if self.gender
      self.gender.downcase == "un homme" ? true : false
    else
      nil
    end
  end


end
