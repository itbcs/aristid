class AnswerIncrementSequence
  include Mongoid::Document
  field :seq_name, type: String
  field :number, type: Integer
end
