class Customer
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Phony
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable
  validates :cgu, acceptance: { accept: [true,1,"true"] }
  validates :phone_number, phony_plausible: true
  validates :phone_number, presence: true
  validates :lastname, presence: true
  validates :city, presence: true

   # Normalizes the attribute itself before validation
  phony_normalize :phone_number, default_country_code: 'FR'

  has_one :survey, dependent: :destroy

  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time
  field :remember_created_at, type: Time
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  field :firstname, type: String
  field :lastname, type: String
  field :city, type: String
  field :phone_number, type: String
  field :cgu, type: Boolean

end
