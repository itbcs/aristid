class Agency
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Phony

  has_one :custom_style
  has_many :surveys
  has_many :agents

  field :name, type: String
  field :photo_url, type: String
  field :website_url, type: String
  field :phone, type: String
  field :facebook_url, type: String
  field :email, type: String
  field :contact_email, type: String
  field :aristid_email, type: String
  field :aristid_url, type: String

end
